# Hints

- If you expierence stuttering in certin intervals disable soft journaling with `tunefs -j disable /dev/ada0` for example.

- When you use ZFS its recommended to disable atime (default), prefetching and adjusting the ARC.

## Esync is only present in wine-staging.

Games like Days Gone might do run horrible without it, however from my POV there is nothing what i can do.

`Neither esync nor fsync will be accepted into upstream Wine.`

## Installation of optional gstreamer-plugins

Some games do need additonal installations of gstreamer1-plugins.

Most games do need for cutscenes: gstreamer1-plugins-openh264

Games with WMA audio like: Skyrim, Fallout 4: gstreamer1-libav

## Increasing virtual memory allocation of 32-Bit games to 4GB on 64-Bit systems.

In case your 32-Bit game is unstable, it might help to increase the virtual memory allocation of the game executable using 4GB Patch (https://ntcore.com/4gb-patch).

# Current problem or limitations

## FreeBSD-CURRENT, AMD64 and i386

Whilst the FreeBSD Project does build packages for `FreeBSD:15:i386`, the relevant listing is not visible at <https://pkg.freebsd.org/>.

So, we see _n/a_ -- not applicable -- for _i386_ at <https://www.freshports.org/emulators/wine/#packages>.

<https://www.freshports.org/faq.php#abi> explains how FreshPorts interprets the `pkg.freebsd.org` front page.

## Not always the best performance (Gone? Not able to reproduce it, anymore.)

It can happen that the performance is different upon game initialization.

However the case to get the best performance is a bit a random and can have serval levels. 

But this doesn't get frustrating as you hit Level 1 quite often.

-   Level 1 (Very Good): very Good performance, no issues. You can hear the GPU fans very well
-   Level 2 (Good) Performance is great but not the best. You can hear the GPU fans well
-   Level 3 (Good but bad) Great performance but its stuttering and the game can even hang up. (Only seem to occur on 1080p?)
-   Level 4 (Bad performance) The game is running at 20 FPS or lower.

## drm-515-kmod from ports is unstable

If you expierence panics while playing or even during normal desktop usage use the following branch: https://github.com/wulf7/drm-kmod/tree/5.15-lts-focal.

https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=274770

## FreeSync/VRR doesnt work correctly with drm 6.1/6.6

https://github.com/freebsd/drm-kmod/issues/298

Workaround: Enable Vsync

## Unity games do lose input when not focused

## Issues with multi threads in many games

Many games have issues while launching or freeze, crashing when entering gameplay. This is to due that they are only can spawn one dispatcher because they are unable to get core numbers correctly as far as i understand.

The issues have been addressed with https://gitlab.winehq.org/wine/wine/-/merge_requests/5213.

Reported to upstream as https://bugs.winehq.org/show_bug.cgi?id=56157.

You can recompile it without issues (currently).

Some engines like Unreal Engine allow running games single-threaded like with `-ONETHREAD` however performance is very limited.

## /proc have to be mounted for some Steam (Windows) games.

/proc is going to be depcrecated however wine still makes usage of it while launching games through Windows Steam for some reason.

If you have not done it yet, please do the following:

	mount -t procfs proc /proc

To make it permanent, you need the following lines in /etc/fstab:

	proc	/proc		procfs		rw	0	0


