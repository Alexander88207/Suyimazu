# Framework

This is a small write what you need to use Mizuma's framework.

Please keep in mind that this is currently incomplete and covers only the bare essentials.

# Preparing installation

## Specify application type

If your application is an game/games-launcher etc.. use `Game=Yes`

If your application is an program, not directly a game etc... use `Program=Yes`

If nothing is specified Mizutamari will not continue your installation.

## Use 32-Bit prefix instead of an WoW64(32+64-Bit) one.

In some cases some 32-Bit applications run better when they are in an 32-bit prefix instead of an WoW64 one.

For example when you want to run an 32-Bit application that requieres .Net components that are mostly only working in 32-Bit enviroments.

If you want to use an 32-Bit prefix for your application use `NoWoW64=Yes`

# Installation

## Downloads

With `DownloadSite` you can specify the address from where the file should be downloaded.

With `DownloadFile` you can specify the files that should be downloaded from `DownloadSite`

If the name of the downloaded file mismatches from `DownloadFile` you can set additionally `SetupFile` to fix it.

If you dont need to download a file in your case then use `NoDownload=Yes`

## Archive

In the most cases the downloads are mostly setups and Mizutamari assumes that the `DownloadFile` can be easily executed.

With `Extract=Yes` you can tell that your `DownloadFile` is an archive and have to be extracted.

If your Setup is behind an archive set additionally `SetupFile`

If your application only needs to be extracted use additionally `NoSetup`

If you need to extract your archive to an special place you can set additionally `ExtractTo` to specify an place that is inside the Mizutamari folder.

## Installing additionally components

With `Winetricks` you can set the additionally winetricks components that should be installed in your application prefix.

With `DXVK=Yes` you can enable an question if DXVK should be installed or not.

## Launching

Use `EXE` to specify the exectuable that should be exectuted inside the application prefix.

Use `WRKDIR` to specify the folder from where `EXE` should be exectuted. Some games cant find it own files if they are not getting executed in the same folder there the own files from the application are it.






