![Banner](https://codeberg.org/Alexander88207/Suyimazu/raw/branch/main/Media/Banner.png)

## Description

Mizutamari is a [Wine](https://www.winehq.org/) front-end. Written in Bash with a GUI provided by Zentiy.

A [predefined library](https://codeberg.org/Alexander88207/Mizutamari/src/branch/main/Library) is used to manage the Windows applications.

There are also the following additional features:

- Automatic disabling of the compositor when starting an gaming application

- Automatic handling of the 32-bit package installation using pkg32.sh

- Convenient use of tools like winetricks, 4GB Patch...

- Switching to wine-proton (Does not receive any support see #Support)

and many more.

It was primarily written for FreeBSD, but with some modifications
it is also possible to run it on other systems without any problems.

## Privacy Note

Mizutamari does not collect any user data.

## Current status

It's finished and changes are being made for maintenance.

## Install- and Uninstallation

- To install Mizutamari:   `pkg install mizuma`

- To uninstall Mizutamari: `pkg remove mizuma`

- To also clear Mizutamari's (your) user data : `rm -r -d $HOME/.local/share/Mizutamari` and `rm $HOME/.config/Mizutamari.conf`

## Usage

To open the main gui type: `Mizuma`

![Screenshot](https://codeberg.org/Alexander88207/Mizutamari/raw/branch/main/Media/Screenshot_2025-01-03_22-01-39.png)

For quick commandline tasks you can use the following examples:

To install something use:         `Mizuma Install Skype`

To launch something use:          `Mizuma Launch Skype`

To uninstall something use: 	    `Mizuma Uninstall Skype`

## Support

Hints, current problems or limitations that you might have to deal with on FreeBSD can viewed [here](https://codeberg.org/Alexander88207/Mizutamari/src/branch/main/Docs/Hints-Current-Problems-Limitations.md).

To ensure that everyone has the same experience and because of the level of effort and time, i only support the stable wine series that is available through the [FreeBSD Ports](https://www.freshports.org/emulators/wine) and consumed with FreeBSD. 

- But i am open to contributions containing <a href="https://codeberg.org/Alexander88207/Mizutamari/src/branch/main/Library">library</a> patches to maintain other wine versions (such as -devel or -proton) if they don't get in the way.

It is recommended that you keep your software up to date, especially for desktop use.
FreeBSD releases are defaulting to the quarterly repository (branch) where you only get bug and security fixes.

- To receive weekly package updates you have to switch to the latest repository. You can read more about that [here](https://wiki.freebsd.org/Ports/QuarterlyBranch).

If you have any problems or questions just open an issue here. 

But i can also be messaged at:

- [FreeBSD Discord](https://discord.com/invite/freebsd)

- [FreeBSD Matrix Group](https://matrix.to/#/#FreeBSD:matrix.org)

## Contributors

Please note that this is a passion project rather than production-grade development.

Indirect contributions such as donations and the like are not required and will be rejected.

There is always a door open for new ideas or improvements, just create an PR (Pull-Request) with your contribution.

See contributors [here](https://codeberg.org/Alexander88207/Mizutamari/activity/contributors)

## Special Thanks

I am very thankful and appreciate the work of people who take care of and improve Wine for FreeBSD.

The current maintainer of Wine is looking for a new maintainer. While the port is still maintained superficially, it benefits much from the community contributions.

If you are interested, simply [create a ticket in the FreeBSD bugzilla](https://bugs.freebsd.org/bugzilla/enter_bug.cgi?component=Individual%20Port%28s%29&product=Ports%20%26%20Packages&short_desc=emulators%2Fwine).